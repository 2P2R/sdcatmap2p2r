--
-- PostgreSQL database dump
--

-- Dumped from database version 11.7
-- Dumped by pg_dump version 11.7

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;
SET search_path TO "$user", public;
--
-- Name: public; Type: SCHEMA; Schema: -; Owner: postgres
--

CREATE SCHEMA public;


ALTER SCHEMA public OWNER TO postgres;

--
-- Name: SCHEMA public; Type: COMMENT; Schema: -; Owner: postgres
--

COMMENT ON SCHEMA public IS 'standard public schema';


--
-- Name: add_length_axe(); Type: FUNCTION; Schema: public; Owner: plage
--

CREATE FUNCTION public.add_length_axe() RETURNS trigger
    LANGUAGE plpgsql
    AS $$DECLARE
length_axe_original float;
length_axe_rounded integer;
BEGIN


select ST_length(NEW.geom,false)  INTO length_axe_original ;
--RAISE EXCEPTION 'Geom --> %, %, %', NEW.geom, length_axe_original ,NEW.id;
length_axe_rounded  := cast(length_axe_original as INTEGER);
--EXECUTE 'UPDATE "AxesCyclables" SET length_meters = ' || length_axe_rounded  || ' where id = '|| NEW.id; 
--EXECUTE 'UPDATE "AxesCyclables" SET length_meters = 26002 where id = 42';
NEW.length_meters := length_axe_rounded  ;
--RAISE EXCEPTION 'Geom --> %, %, %', NEW.geom, length_axe_rounded  ,NEW.id;
NEW.modification_date = now();
return NEW;
END$$;


ALTER FUNCTION public.add_length_axe() OWNER TO plage;

--
-- Name: lizmap_author(); Type: FUNCTION; Schema: public; Owner: plage
--

CREATE FUNCTION public.lizmap_author() RETURNS trigger
    LANGUAGE plpgsql
    AS $$BEGIN
--RAISE NOTICE 'TG_OP: %, NEW.author_name = %, OLD.author_name = %', TG_OP, NEW.author_name,OLD.author_name;

IF TG_OP='INSERT' THEN
    NEW.last_modified_by := NEW.author_name;
ELSE 
IF TG_OP='UPDATE' THEN
    NEW.last_modified_by := NEW.author_name;
    NEW.author_name := OLD.author_name;
END IF;
END IF;

return NEW;
END;$$;


ALTER FUNCTION public.lizmap_author() OWNER TO plage;

--
-- Name: update_last_modifcation_date(); Type: FUNCTION; Schema: public; Owner: plage
--

CREATE FUNCTION public.update_last_modifcation_date() RETURNS trigger
    LANGUAGE plpgsql
    AS $$DECLARE



BEGIN



NEW.modification_date = now();

return NEW;

END$$;


ALTER FUNCTION public.update_last_modifcation_date() OWNER TO plage;

--
-- Name: AxesCyclables_id_seq2; Type: SEQUENCE; Schema: public; Owner: plage
--

CREATE SEQUENCE public."AxesCyclables_id_seq2"
    START WITH 87
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."AxesCyclables_id_seq2" OWNER TO plage;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: AxesCyclables; Type: TABLE; Schema: public; Owner: plage
--

CREATE TABLE public."AxesCyclables" (
    id integer DEFAULT nextval('public."AxesCyclables_id_seq2"'::regclass) NOT NULL,
    intitule_axe character varying(255) NOT NULL,
    length_meters integer,
    geom public.geometry(LineString,4326),
    author_name character varying(255),
    creation_date timestamp without time zone DEFAULT now(),
    modification_date timestamp without time zone DEFAULT now(),
    statut character(22),
    lien_argumentaire text,
    priorite character varying(255) NOT NULL,
    last_modified_by character varying(25),
    dossier character varying(500),
    commentaire text
);


ALTER TABLE public."AxesCyclables" OWNER TO plage;

--
-- Name: CompteursVelos; Type: TABLE; Schema: public; Owner: plage
--

CREATE TABLE public."CompteursVelos" (
    id integer NOT NULL,
    description text NOT NULL,
    geom public.geometry(Point),
    name character varying(255) NOT NULL
);


ALTER TABLE public."CompteursVelos" OWNER TO plage;

--
-- Name: CompteursVelos_id_seq; Type: SEQUENCE; Schema: public; Owner: plage
--

CREATE SEQUENCE public."CompteursVelos_id_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."CompteursVelos_id_seq" OWNER TO plage;

--
-- Name: CompteursVelos_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: plage
--

ALTER SEQUENCE public."CompteursVelos_id_seq" OWNED BY public."CompteursVelos".id;


--
-- Name: ReseauExpressVelo; Type: TABLE; Schema: public; Owner: plage
--

CREATE TABLE public."ReseauExpressVelo" (
    id integer NOT NULL,
    intitule_axe character varying(255) NOT NULL,
    length_meters integer,
    geom public.geometry(LineString,4326),
    author_name character varying(255),
    creation_date timestamp without time zone DEFAULT now(),
    modification_date timestamp without time zone DEFAULT now(),
    statut character(22),
    lien_argumentaire text,
    priorite character varying(255) NOT NULL,
    last_modified_by character varying(25),
    couleur character varying(13),
    dossier character varying(500),
    commentaire text
);


ALTER TABLE public."ReseauExpressVelo" OWNER TO plage;

--
-- Name: ReseauExpressVelo_id_seq; Type: SEQUENCE; Schema: public; Owner: plage
--

CREATE SEQUENCE public."ReseauExpressVelo_id_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."ReseauExpressVelo_id_seq" OWNER TO plage;

--
-- Name: ReseauExpressVelo_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: plage
--

ALTER SEQUENCE public."ReseauExpressVelo_id_seq" OWNED BY public."ReseauExpressVelo".id;


--
-- Name: UrbanismeTactique_id_seq; Type: SEQUENCE; Schema: public; Owner: plage
--

CREATE SEQUENCE public."UrbanismeTactique_id_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."UrbanismeTactique_id_seq" OWNER TO plage;

--
-- Name: UrbanismeTactique; Type: TABLE; Schema: public; Owner: plage
--

CREATE TABLE public."UrbanismeTactique" (
    id integer DEFAULT nextval('public."UrbanismeTactique_id_seq"'::regclass) NOT NULL,
    intitule_axe character varying(255) NOT NULL,
    description text NOT NULL,
    type_amenagement character varying(255) NOT NULL,
    length_meters integer,
    geom public.geometry(LineString,4326),
    author_name character varying(255),
    creation_date timestamp without time zone DEFAULT now(),
    modification_date timestamp without time zone DEFAULT now(),
    statut character(40) NOT NULL,
    argumentaire text NOT NULL,
    priorite character varying(255) NOT NULL,
    last_modified_by character varying(25)
);


ALTER TABLE public."UrbanismeTactique" OWNER TO plage;

--
-- Name: point; Type: TABLE; Schema: public; Owner: plage
--

CREATE TABLE public.point (
    id integer NOT NULL,
    nom character varying(255) NOT NULL,
    description character varying(255) NOT NULL,
    geom public.geometry(Point,4326),
    nb_personnes integer
);


ALTER TABLE public.point OWNER TO plage;

--
-- Name: point_id_seq; Type: SEQUENCE; Schema: public; Owner: plage
--

CREATE SEQUENCE public.point_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.point_id_seq OWNER TO plage;

--
-- Name: point_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: plage
--

ALTER SEQUENCE public.point_id_seq OWNED BY public.point.id;


--
-- Name: CompteursVelos id; Type: DEFAULT; Schema: public; Owner: plage
--

ALTER TABLE ONLY public."CompteursVelos" ALTER COLUMN id SET DEFAULT nextval('public."CompteursVelos_id_seq"'::regclass);


--
-- Name: ReseauExpressVelo id; Type: DEFAULT; Schema: public; Owner: plage
--

ALTER TABLE ONLY public."ReseauExpressVelo" ALTER COLUMN id SET DEFAULT nextval('public."ReseauExpressVelo_id_seq"'::regclass);


--
-- Name: point id; Type: DEFAULT; Schema: public; Owner: plage
--

ALTER TABLE ONLY public.point ALTER COLUMN id SET DEFAULT nextval('public.point_id_seq'::regclass);


--
-- Name: CompteursVelos CompteursVelos_pkey; Type: CONSTRAINT; Schema: public; Owner: plage
--

ALTER TABLE ONLY public."CompteursVelos"
    ADD CONSTRAINT "CompteursVelos_pkey" PRIMARY KEY (id);


--
-- Name: point id_PK; Type: CONSTRAINT; Schema: public; Owner: plage
--

ALTER TABLE ONLY public.point
    ADD CONSTRAINT "id_PK" PRIMARY KEY (id);


--
-- Name: AxesCyclables pk; Type: CONSTRAINT; Schema: public; Owner: plage
--

ALTER TABLE ONLY public."AxesCyclables"
    ADD CONSTRAINT pk PRIMARY KEY (id);


--
-- Name: UrbanismeTactique pk_urbanismetactique; Type: CONSTRAINT; Schema: public; Owner: plage
--

ALTER TABLE ONLY public."UrbanismeTactique"
    ADD CONSTRAINT pk_urbanismetactique PRIMARY KEY (id);


--
-- Name: ReseauExpressVelo rev_pk; Type: CONSTRAINT; Schema: public; Owner: plage
--

ALTER TABLE ONLY public."ReseauExpressVelo"
    ADD CONSTRAINT rev_pk PRIMARY KEY (id);


--
-- Name: idx_point_Nom; Type: INDEX; Schema: public; Owner: plage
--

CREATE INDEX "idx_point_Nom" ON public.point USING btree (nom);


--
-- Name: idx_point_id; Type: INDEX; Schema: public; Owner: plage
--

CREATE INDEX idx_point_id ON public.point USING btree (id);


--
-- Name: sidx_CompteursVelos_geom; Type: INDEX; Schema: public; Owner: plage
--

CREATE INDEX "sidx_CompteursVelos_geom" ON public."CompteursVelos" USING gist (geom);


--
-- Name: sidx_point_geom; Type: INDEX; Schema: public; Owner: plage
--

CREATE INDEX sidx_point_geom ON public.point USING gist (geom);


--
-- Name: AxesCyclables add_last_modified_by; Type: TRIGGER; Schema: public; Owner: plage
--

CREATE TRIGGER add_last_modified_by BEFORE INSERT OR UPDATE ON public."AxesCyclables" FOR EACH ROW EXECUTE PROCEDURE public.lizmap_author();


--
-- Name: ReseauExpressVelo add_last_modified_by; Type: TRIGGER; Schema: public; Owner: plage
--

CREATE TRIGGER add_last_modified_by BEFORE INSERT OR UPDATE ON public."ReseauExpressVelo" FOR EACH ROW EXECUTE PROCEDURE public.lizmap_author();


--
-- Name: UrbanismeTactique add_last_modified_by; Type: TRIGGER; Schema: public; Owner: plage
--

CREATE TRIGGER add_last_modified_by BEFORE INSERT OR UPDATE ON public."UrbanismeTactique" FOR EACH ROW EXECUTE PROCEDURE public.lizmap_author();


--
-- Name: AxesCyclables add_length_axe; Type: TRIGGER; Schema: public; Owner: plage
--

CREATE TRIGGER add_length_axe BEFORE INSERT OR UPDATE ON public."AxesCyclables" FOR EACH ROW EXECUTE PROCEDURE public.add_length_axe();


--
-- Name: ReseauExpressVelo add_length_axe; Type: TRIGGER; Schema: public; Owner: plage
--

CREATE TRIGGER add_length_axe BEFORE INSERT OR UPDATE ON public."ReseauExpressVelo" FOR EACH ROW EXECUTE PROCEDURE public.add_length_axe();


--
-- Name: UrbanismeTactique add_length_axe; Type: TRIGGER; Schema: public; Owner: plage
--

CREATE TRIGGER add_length_axe BEFORE INSERT OR UPDATE ON public."UrbanismeTactique" FOR EACH ROW EXECUTE PROCEDURE public.add_length_axe();


--
-- PostgreSQL database dump complete
--


