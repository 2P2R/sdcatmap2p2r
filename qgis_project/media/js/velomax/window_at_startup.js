lizMap.events.on({

  layersadded: function(e) {
    var html = '';
    html+= '<div class="modal-header"><a class="close" data-dismiss="modal">×</a><h3>VeloMAX (V&eacute;lo Maillage AXes cyclables)</h3></div>';

    html+= '<div class="modal-body">';
    //html+= $('#metadata').html();
        html+= 'Bienvenu(e) sur l\'application cartographique mise en place par l\'association 2P2R pour contribuer &agrave; l\'&eacute;laboration du sch&eacute;ma directeur de l\'agglom&eacute;ration toulousaine, port&eacute;e par Tiss&eacute;o Collectivit&eacute;s<br/>';
        html+= 'Vous trouverez sur cette carte les propositions des b&eacute;n&eacute;voles de 2P2R qui se sont mobilis&eacute;s pendant plusieurs mois<br/>';
        html+='<ul><li>Un r&eacute;seau express v&eacute;lo (REV) couvrant l\'ensemble de l\'agglom&eacute;ration (8 axes)</li>';
        html+='<li>Un maillage plus local, continu pour desservir des zones d\'emplois et de vie (320 axes)</li></ul>';
        html+='Chaque axe dessin&eacute; sur la carte est associ&eacute; &agrave; un dossier descriptif au format pdf, argumentant la demande. <strong>Pour y acc&eacute;der, il suffit <ul><li>de cliquer sur un axe de la carte</li><li>dans le formulaire qui s\'affiche &agrave; gauche, cliquer sur le lien du dossier.</li></ul></strong>';
        html+='La version de VeloMax de janvier 2019 est accessible sur <a href="http://umap.openstreetmap.fr/fr/map/proposition-traces-schema-directeur-cyclable-2p2r-_383161#11/43.6097/1.3857" target="_blank">umap</a>.';
    html+= '</div>';

    html+= '<div class="modal-footer"><button type="button" class="btn btn-default" data-dismiss="modal">Ok</button></div>';

    $('#lizmap-modal').html(html).modal('show');

  }

});